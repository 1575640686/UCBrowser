package com.kevin.ucbrowser.ui;

import android.app.Activity;

public abstract class BaseActivity extends Activity {
	
	public abstract void findView();
}
